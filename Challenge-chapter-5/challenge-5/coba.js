// //-----Mulai mengerjakan CHALLENGE CHAPTER 5------

const express = require("express");
const app = express();
// let posts = require("./posts.json");
const port = 3000;

//gunakan EJS
app.set("view engine", "ejs");

// // ----Melakukan routing
app.get("/", (req, res) => {
  // res.sendFile("/page1.html", { root: __dirname });
  res.render("page1");
});

app.get("/page2", (req, res) => {
  // res.sendFile("/page2.html", { root: __dirname });
  res.render("page2");
});

app.get("/page3", (req, res) => {
  // res.sendFile("/page3.html", { root: __dirname });
  res.render("page3");
});

app.get("/page4", (req, res) => {
  // res.sendFile("/page4.html", { root: __dirname });
  res.render("page4");
});
app.get("/page5", (req, res) => {
  // res.sendFile("/page5.html", { root: __dirname });
  res.render("page5");
});

app.get("/page6", (req, res) => {
  // res.sendFile("/suwitjepang.html", { root: __dirname });
  res.render("suwitjepang");
});

app.use("/", (req, res) => {
  res.status(404);
  res.send("<h1>404</h1>");
});

// //-----Membuat API-----

// //get all data
// // app.get("/api/v1/posts", (req, res) => {
// //   res.status(200).json(posts);
// // });

// // endpoint: http://localhost:3000/api/v1/posts for get all data, gunakan method GET

// // GET data by id
// // app.get("/api/v1/posts/:id", (req, res) => {
// //   const post = posts.find((i) => i.id === +req.params.id);
// //   res.status(200).json(post);
// // });
// // endpoint: http://localhost:3000/api/v1/posts/1 for get data by id, method GET

// // GET data by id
// // app.get("/api/v1/posts/:id", (req, res) => {
// //   const post = posts.find((i) => i.id === +req.params.id);
// //   res.status(200).json(post);
// // });
// // endpoint: http://localhost:3000/api/v1/posts/1 for get data by id, method GET
// // POST
// // app.post("/api/v1/posts", (req, res) => {
// //   /**
// //    * Menghandler request body,
// //    * memanggil dengan req.body
// //    */
// //   const { title, author, price, stock } = req.body;

// //   // GET id
// //   const id = posts[posts.length - 1].id + 1;
// //   const post = {
// //     id,
// //     title,
// //     author,
// //     price,
// //     stock,
// //   };

// //   // Simpan ke dalam bentuk array
// //   posts.push(post);
// //   res.status(201).json(post);
// // });
// // // endpoint: http://localhost:3000/api/v1/posts for post data, gunakan method POST

// // // PUT
// // // app.put("/api/v1/posts/:id", (req, res) => {
// // //   let post = posts.find((i) => i.id === +req.params.id);

// // //   const params = {
// // //     title: req.body.title,
// // //     author: req.body.author,
// // //     price: req.body.price,
// // //     stock: req.body.stock,
// // //   };
// // //   post = { ...post, ...params };

// // //   post = posts.map((i) => (i.id === post.id ? post : i));

// // //   res.status(200).json(post);
// // // });
// // // endpoint: http://localhost:3000/api/v1/posts/1 for update date, gunakan method PUT

// // // DELETE
// // // app.delete("/api/v1/posts/:id", (req, res) => {
// // //   posts = posts.filter((i) => i.id !== +req.params.id);

// // //   res.status(200).json({
// // //     message: `Post dengan id ${req.params.id} sudah berhasil dihapus`,
// // //   });
// // // });
// // // endpoint: http://localhost:3000/api/v1/posts/1 for delete data, gunakan method DELETE

app.listen(port, () => console.log(`example app listening at http://${port}`));
