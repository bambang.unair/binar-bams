/** Membuat tabel user_game*/
CREATE TABLE user_game(id BIGSERIAL PRIMARY KEY, nama VARCHAR(45) NOT NULL, username VARCHAR(10) NOT NULL, email TEXT NOT NULL);

/* Membuat data tabel user_game*/
INSERT INTO user_game(nama, username, email)VALUES ('Bambang','bams','bambang@gmail.com');
INSERT INTO user_game(nama, username, email)VALUES ('dodi','dods','dodi@gmail.com');
INSERT INTO user_game(nama, username, email)VALUES ('dina','dina','dina@gmail.com');
INSERT INTO user_game(nama, username, email)VALUES ('erik','riks','erik@gmail.com');
INSERT INTO user_game(nama, username, email)VALUES ('bombom','boms','boms@gmail.com');

/*Melihat isi tabel*/
SELECT * FROM user_game;

/* Membuat tabel user_game_biodata*/
CREATE TABLE user_game_biodata(id BIGSERIAL PRIMARY KEY, nama VARCHAR(45) NOT NULL, alamat VARCHAR(50) NOT NULL, telepon TEXT NOT NULL);

/* Membuat data tabel user_game_biodata*/
INSERT INTO user_game_biodata(nama, alamat, telepon)VALUES ('Bambang','Gresik','0812345678');
INSERT INTO user_game_biodata(nama, alamat, telepon)VALUES ('dodi','surabaya','08213243546');
INSERT INTO user_game_biodata(nama, alamat, telepon)VALUES ('dina','mojokerto','085732165476');
INSERT INTO user_game_biodata(nama, alamat, telepon)VALUES ('erik','lamongan','083876543267');
INSERT INTO user_game_biodata(nama, alamat, telepon)VALUES ('bombom','jakarta','08986756435');

/**membuat tabel user_game_history*/
CREATE TABLE user_game_history(id BIGSERIAL PRIMARY KEY, score VARCHAR(45) NOT NULL, waktu VARCHAR(10) NOT NULL);

/** Membuat data tabel user_games_history*/
INSERT INTO user_game_history(score, waktu)VALUES(8, 30);
INSERT INTO user_game_history(score, waktu)VALUES(7, 40);
INSERT INTO user_game_history(score, waktu)VALUES(8, 45);
INSERT INTO user_game_history(score, waktu)VALUES(9, 30);
INSERT INTO user_game_history(score, waktu)VALUES(9, 60);