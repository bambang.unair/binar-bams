// impelementasi in express
//npm install ejs

const express = require("express");
const app = express();
const path = require("path");
const { Users } = require("./models");

app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");

app.use("/assets", express.static(path.join(__dirname, "assets")));

// menampilkan halaman login
app.get("/", (req, res) => {
  res.render("login");
});

// get all articles
app.get("/home", (req, res) => {
  Users.findAll().then((users) => {
    res.render("users", {
      users,
    });
  });
});

// Menampilkan form tambah user
app.get("/users/create", (req, res) => {
  res.render("users/create");
});

// post tambah user
app.post("/users", (req, res) => {
  Users.create({
    username: req.body.username,
    email: req.body.email,
  }).then((users) => {
    res.send("user berhasil dibuat");
  });
});

// get message success
app.get("/users/create", (req, res) => {
  res.render("users/create");
});

// UPDATE USER
app.get("/users/edit/(:id)", (req, res) => {
  Users.findOne({
    where: { id: req.params.id },
  }).then((users) => {
    res.render("users/edit");
  });
});

app.listen(3000, () => {
  console.log("server running at port 3000");
});
