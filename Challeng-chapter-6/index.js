// mengimpor modul express

const express = require("express");
const app = express();
const { Users } = require("./models");

// menggunakan JSON
app.use(express.json());

// ---REST API menggunakan JSON

// GET ALL USERS :msih gagal saat check di POSTMAN
app.get("/users", (req, res) => {
  Users.findAll().then((users) => {
    res.status(200).json(users);
  });
});

// GET By Id : berhasil cek di POST MAN
app.get("/users/:id", (req, res) => {
  Users.findOne({
    where: { id: req.params.id },
  }).then((users) => {
    res.status(200).json(users);
  });
});

// POST AN USER GAME : masih gagal operasi di postman
app.post("/users", (req, res) => {
  Users.create({
    username: req.body.username,
    email: req.body.email,
  })
    .then((users) => {
      res.status(201).json(users);
    })
    .catch((err) => {
      res.status(422).json("can't create an user game");
    });
});

// UPDATE an User Game : berhasil di postman
app.put("/users/:id", (req, res) => {
  Users.update(
    {
      username: req.body.username,
      password: req.body.email,
    },
    {
      where: { id: req.params.id },
    }
  )
    .then((users) => {
      res.status(201).json(users);
    })
    .catch((err) => {
      res.status(422).json("Can't update an user_game");
    });
});

// Delete an users
app.delete("/users/:id", (req, res) => {
  Users.destroy({
    where: { id: req.params.id },
  }).then((users) => {
    res.status(200).json({
      message: `User game dgn id ${req.params.id} sudah dihapus`,
    });
  });
});

// Menyalakan server
app.listen(3000, () => {
  console.log("Server running at port 3000");
});
